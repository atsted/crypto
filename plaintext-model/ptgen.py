import sys
import random
from bisect import bisect_left

kgrams = []

def get_random_in(arr, p):
  r = random.random() * p[len(p) - 1]
  return arr[bisect_left(p, r) - 1]

def get_next_symbol(k=2, prev=''):
  p = [0]
  fulls = kgrams[k - 1]
  parts = kgrams[k - 2]
  p_prev = parts.get(prev)
  if p_prev is None: return None
  for a in alphabet:
    x = fulls.get(prev + a)
    p_full = x if x is not None else 0
    p.append(p[len(p) - 1] + p_full / p_prev)
  return get_random_in(alphabet, p)

def compute_stats(filename=''):
  # counting
  for line in open(filename):
    for i in range(1, k + 1):
      if len(kgrams) < i:
        kgrams.append({ 'total': 0 })
      for j in range(0, len(line) - i + 1):
        kgram = line[j:j + i]
        num = kgrams[i - 1].get(kgram)
        kgrams[i - 1][kgram] = 1 if num == None else num + 1
        kgrams[i - 1]['total'] += 1
  # normalization
  for table in kgrams:
    t = table['total']
    for kgram in table:
      table[kgram] *= 0.5 / t

def count_start():
  p = [0]
  table = kgrams[k - 2]
  parts = [ x for x in table if x is not 'total' ]
  for kgram in table:
    p.append(p[len(p) - 1] + table[kgram])
  return get_random_in(parts, p)

def create_plaintext(n=666, k=2):
  i = 1
  plaintext = count_start()
  while i < n:
    prev = plaintext[i - 1:]
    symbbol = get_next_symbol(k, prev)
    if symbbol is not None:
      plaintext += symbbol
    i += 1
  return plaintext

if __name__ == '__main__':
  n = int(sys.argv[1])
  k = int(sys.argv[2])
  compute_stats('origin.txt')
  alphabet = [ a for a in kgrams[0] if a is not 'total' ]
  plaintext = create_plaintext(n, k)
  print(plaintext)
