#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

enum class Style { science, fiction };

typedef std::map<std::string, double> kGrams;

std::vector<kGrams> tables;
std::vector<std::string> files{ "science.txt", "fiction.txt" };

void displayHelp();
void countStatistics(int, auto);
void generatePlanetext(auto &, int, int);

int main(int argc, char *argv[]) {
	int k = 1;
	std::string plaintext;
	if (argc < 2) { displayHelp(); return (1); }
	if (argc > 2) { k = atoi(argv[2]); }
	countStatistics(k, Style::fiction);
	generatePlanetext(plaintext, atoi(argv[1]), k);
	std::cout << plaintext << std::endl;
	return (0);
}

void displayHelp() {
	std::cout <<
		"OVERVIEW:\n"\
		"  ptgen - program generates n characters of plaintext\n"\
		"          using k-grams probabilistic model\n\n"\
		"OPTIONS:\n"\
		"  n\t- positive integer, is reqiured\n"\
		"  k\t- positive integer, is equal to 1 by default\n\n"\
		"USAGE:\n"\
		"  ptgen 666 6\n\n";
}

void countStatistics(int k, auto style) {
	int total = 0;
	const auto fname = files[(int)style];
	std::ifstream f(fname);
	std::string line;
	tables.resize(k);
	// std::getline(f, line);
	// std::cout << line << std::endl;
	// std::cout << line.substr(0, 9) << std::endl;
	// std::cout << line.substr(0, 10) << std::endl;

	char kgram[];
	while (std::getline(f, line)) {
		int len = line.length();
		total += len;
		for (int i = 0; i < k; i++) {
			for (int j = 0; j < len - i; j++) {
				// std::string kgram = line.substr(j, 2 * i);
				std::cout << kgram << std::endl;
				tables[i][kgram]++;
			}
		}
	}
	f.close();
	// for (auto &table : tables) {
	//   for (auto it = table.begin(); it != table.end(); it++) {
	//     std::cout << it->first << " " << it->second << std::endl;
	//   }
	// }
}

void generatePlanetext(auto &str, int len, int k) {
	if ((len <= 0) || (k <= 0)) { return; }

}
